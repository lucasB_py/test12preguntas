package com.example.test12preguntas;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    /*RECURSOS GLOBALES*/
    private int id_respuestas[] = {
            //Arreglo de las 4 opciones de respuesta
            R.id.respuesta1, R.id.respuesta2,
            R.id.respuesta3, R.id.respuesta4
    };
    private int respuesta_correcta;
    private int pregActual;
    private String[] preguntas;
    private boolean[] resEsCorrecta;
    private int[] resGuardada;
    private TextView txt_pregunta;
    private RadioGroup rbgrupo;
    private Button btnNext, btnPrev;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt_pregunta = (TextView) findViewById(R.id.txt_pregunta);
        rbgrupo = (RadioGroup) findViewById(R.id.respuestas_grupo);
        btnNext = (Button) findViewById(R.id.btnCheck);
        btnPrev = (Button) findViewById(R.id.btn_prev);

        preguntas = getResources().getStringArray(R.array.preguntas);
        iniciarRe();

        btnNext.setOnClickListener((v) -> {
            revisarRespuestas();
            if (pregActual < preguntas.length-1){
                pregActual++;
                mostrarPregunta();
            } else {
                revResultado();
            }
        });

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*BOTON PARA REGRESAR A LA PREGUNTA ANTERIOR*/
                revisarRespuestas();
                if (pregActual > 0){
                    pregActual--;
                    mostrarPregunta();
                }
            }
        });
    }

    private void iniciarRe() {
        resEsCorrecta = new boolean[preguntas.length];
        resGuardada = new int[preguntas.length]; //Variable para guardar la pregunta seleccionada anterior, o posterior
        for (int i = 0; i < resGuardada.length; i++) {
            resGuardada[i] = -1;
        }
        pregActual = 0;
        mostrarPregunta();
    }

    private void revResultado() {
        /*METODO PARA REVISAR CUANTAS PREGUNTAS ESTUVIERON MAL, BIEN y NO CONTESTADAS*/
        int bien = 0, mal = 0, nocontestadas = 0;
        for (int i = 0; i < preguntas.length; i++){
            if (resEsCorrecta[i]) bien++;
            else if (resGuardada[i] == -1) nocontestadas++;
            else mal++;
        }

        String mensaje = String.format("Correctas: %d\nIncorrectas: %d\nNo contestadas: %d\n",
                bien, mal, nocontestadas);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.resultados);
        builder.setMessage(mensaje);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.finish, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setNegativeButton(R.string.reiniciarPreg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //BORRAR RESPUESTAS ALMACENADAS Y REINICIAR FORMULARIO
                iniciarRe();
            }
        });
        builder.create().show();
    }

    private void revisarRespuestas() {
        /*METODO PARA REVISAR LAS RESPUESTAS SELECCIONADAS*/
        int id = rbgrupo.getCheckedRadioButtonId();
        int res = -1;

        for(int i = 0; i < id_respuestas.length; i++){
            if(id_respuestas[i] == id) {
                res = i;
            }
        }
        resEsCorrecta[pregActual] = (res == respuesta_correcta);
        resGuardada[pregActual] = res;
    }

    private void mostrarPregunta() {
        /*Metodo para mostrar la pregunta actual con sus
          respectivas respuestas
        */

        String preg0 = preguntas[pregActual];
        String[] particion = preg0.split(";");

        rbgrupo.clearCheck();

        txt_pregunta.setText(particion[0]);
        for(int i = 0; i < id_respuestas.length; i++){
            /*Ciclo for para particionar el arreglo de las respuestas*/

            RadioButton rb = (RadioButton) findViewById(id_respuestas[i]);

            String res = particion[i + 1];
            /*  Si en el arreglo existe el simbolo *
                determinara que esa es la respuesta correcta
            */
            if (res.charAt(0) == '*'){
                respuesta_correcta = i;
                res = res.substring(1);
            }
            rb.setText(res);
            if (resGuardada[pregActual] == i){
                rb.setChecked(true);
            }
        }

        if (pregActual == 0){
            btnPrev.setVisibility(View.GONE);
        } else {
            btnPrev.setVisibility(View.VISIBLE);
        }
        if (pregActual == preguntas.length-1){
            btnNext.setText(R.string.finish);
        } else {
            btnNext.setText(R.string.next);
        }
    }
}